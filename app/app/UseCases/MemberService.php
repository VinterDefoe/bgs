<?php


namespace App\UseCases;


use App\Events\MemberAddedEvent;
use App\Models\Member;
use Illuminate\Support\Facades\DB;

class MemberService
{
    /**
     * @param array $filter
     * @return \Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection|mixed[]
     */
    public function memberList($filter)
    {
        $eventsIds = $filter['events-ids'] ?? null;

        if ($eventsIds) {
            $eventsIds = array_map('intval', explode(',', $eventsIds));
        }

        return Member::with('events')
            ->when($eventsIds, static function ($query, $eventsIds) {
                $query->whereHas('events', static function ($query) use ($eventsIds) {
                    $query->whereIn('events.id', $eventsIds);
                });
            })->get();
    }

    /**
     * @param $name
     * @param $surname
     * @param $email
     * @param $eventsIds
     * @return mixed
     */
    public function createMemberAndAttachToEvent($name, $surname, $email, $eventsIds)
    {
        $member = DB::transaction(static function () use ($name, $surname, $email, $eventsIds) {
            $member = Member::add($name, $surname, $email);
            $member->events()->attach($eventsIds);
            return $member;
        });

        event(new MemberAddedEvent($member));
        return $member;
    }

    /**
     * @param $memberId
     * @param $name
     * @param $surname
     * @param $email
     * @param $eventsIds
     * @return mixed
     */
    public function updateMemberAndMemberEvents($memberId, $name, $surname, $email, $eventsIds)
    {
        $member = Member::findOrFail($memberId);

        return DB::transaction(static function () use ($member, $name, $surname, $email, $eventsIds) {

            $member->name = $name;
            $member->surname = $surname;
            $member->email = $email;
            $member->save();
            $member->events()->sync($eventsIds);
            return $member;
        });
    }

    /**
     * @param $id
     * @return mixed
     */
    public function deleteMember($id)
    {
        $member = Member::findOrFail($id);

        return DB::transaction(static function () use ($member) {
            $member->events()->detach();
            $member->delete();
        });
    }
}
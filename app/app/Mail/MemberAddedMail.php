<?php

namespace App\Mail;

use App\Models\Member;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class MemberAddedMail extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;
    /**
     * @var Member
     */
    public $member;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Member $member)
    {
        $this->member = $member;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this
            ->to('wewewew@mail.com')
            ->from('adcasdasd@mail.com')
            ->subject('Участник добавлен')
            ->markdown('emails.member-added');
    }
}

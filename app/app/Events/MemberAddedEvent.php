<?php

namespace App\Events;

use App\Models\Member;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class MemberAddedEvent
{
    use Dispatchable, SerializesModels;
    /**
     * @var Member
     */
    public $member;

    /**
     * MemberAddedEvent constructor.
     * @param Member $member
     */
    public function __construct(Member $member)
    {
        $this->member = $member;
    }

}

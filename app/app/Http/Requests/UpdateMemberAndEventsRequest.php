<?php

namespace App\Http\Requests;

use App\Rules\EventsIdsExistRule;
use Illuminate\Foundation\Http\FormRequest;

class UpdateMemberAndEventsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|min:3|max:20',
            'surname' => 'required|string|min:3|max:20',
            'email' => 'required|email|unique:members,email,'.$this->id,
            'events_ids' => ['array', new EventsIdsExistRule()],
        ];
    }

    public function messages()
    {
        return[
            'required.name' => 'Введите имя',
            'required.surname' => 'Введите фамилию',
            'required.email' => 'Заполните email',
            'required.events_ids' => 'Выберите хотя бы один евент',
            'required' => 'поле обязательно для заполнения',
            'name.min' => 'Имя должно быть длиннее двух символов',
            'name.max' => 'Имя должно быть максисум 20 символов',
            'surname.min' => 'Фамилия должна быть длиннее двух символов',
            'surname.max' => 'Фамилия должна быть максисум 20 символов',
            'email.unique' => 'Участник с таким email уже есть',
        ];
    }
}

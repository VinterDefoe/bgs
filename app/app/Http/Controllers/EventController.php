<?php


namespace App\Http\Controllers;


use App\Http\Resources\EventCollection;
use App\Models\Event;

class EventController extends Controller
{
    /**
     * @return EventCollection
     */
    public function eventList(): EventCollection
    {
        return new EventCollection(Event::get());
    }
}
<?php


namespace App\Http\Controllers;


use App\Http\Requests\CreateMemberAndAttachToEventRequest;
use App\Http\Requests\MemberListRequest;
use App\Http\Requests\UpdateMemberAndEventsRequest;
use App\Http\Resources\MemberCollection;
use App\Http\Resources\MemberResource;
use App\UseCases\MemberService;
use Illuminate\Http\JsonResponse;

class MemberEventController extends Controller
{

    /**
     * @var MemberService
     */
    private $memberService;

    public function __construct(MemberService $memberService)
    {
        $this->memberService = $memberService;
    }

    /**
     * @param MemberListRequest $request
     * @return MemberCollection
     */
    public function memberList(MemberListRequest $request): MemberCollection
    {
        return new MemberCollection($this->memberService->memberList($request->all()));
    }

    /**
     * @param CreateMemberAndAttachToEventRequest $request
     * @return MemberResource
     */
    public function createMemberAndAttachToEvent(CreateMemberAndAttachToEventRequest $request): MemberResource
    {
        $name = $request->input('name');
        $surname = $request->input('surname');
        $email = $request->input('email');
        $eventsIds = $request->input('events_ids');

        $member = $this->memberService->createMemberAndAttachToEvent($name, $surname, $email, $eventsIds);
        return new MemberResource($member);
    }

    /**
     * @param UpdateMemberAndEventsRequest $request
     * @param $id
     * @return MemberResource
     */
    public function updateMemberAndMemberEvents(UpdateMemberAndEventsRequest $request, $id): MemberResource
    {
        $name = $request->input('name');
        $surname = $request->input('surname');
        $email = $request->input('email');
        $eventsIds = $request->input('events_ids');

        $member = $this->memberService->updateMemberAndMemberEvents($id, $name, $surname, $email, $eventsIds);
        return new MemberResource($member);
    }

    /**
     * @param $id
     * @return JsonResponse
     */
    public function deleteMember($id): JsonResponse
    {
        $this->memberService->deleteMember($id);
        return response()->json(['success' => true]);
    }

}
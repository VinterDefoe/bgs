<?php

namespace App\Listeners;

use App\Events\MemberAddedEvent;
use App\Mail\MemberAddedMail;
use Illuminate\Support\Facades\Mail;

class MemberAddedListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * @param MemberAddedEvent $event
     */
    public function handle(MemberAddedEvent $event)
    {
        Mail::send(new MemberAddedMail($event->member));
    }
}

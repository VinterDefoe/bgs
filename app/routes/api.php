<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/event-list', 'EventController@eventList');

Route::get('/member-list', 'MemberEventController@memberList');
Route::post('/creat-member-and-attach-to-event', 'MemberEventController@createMemberAndAttachToEvent');
Route::put('/update-member-and-member-events/{id}', 'MemberEventController@updateMemberAndMemberEvents');
Route::delete('/member/{id}', 'MemberEventController@deleteMember');
<?php

namespace Tests\Feature;

use App\Models\Event;
use App\Models\Member;
use App\UseCases\MemberService;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Http\Response;
use Illuminate\Support\Carbon;
use Tests\TestCase;

class MemberControllerTest extends TestCase
{
    use RefreshDatabase;

    public function testupdateMemberAndMemberEvents()
    {

        $eventThree = Event::add(
            'event three',
            'Rostov',
            Carbon::make('12.07.2020')
        );

        $eventFour = Event::add(
            'event four',
            'Rostov',
            Carbon::make('12.07.2020')
        );

        $this->postJson('/api/creat-member-and-attach-to-event', [
            'name' => 'John',
            'surname' => 'Johnsmith',
            'email' => 'johnsmith@mail.com',
            'events_ids' => [$eventThree->id]
        ]);


        $member = Member::whereEmail('johnsmith@mail.com')->first();

        $response = $this->putJson('/api/update-member-and-member-events/' . $member->id, [
            'name' => 'Ivan',
            'surname' => 'smith',
            'email' => 'johnsmith@mail.com',
            'events_ids' => [$eventThree->id, $eventFour->id]
        ]);

        $response->assertStatus(200)
            ->assertExactJson([
                'data' => [
                    'id' => $member->id,
                    'name' => 'Ivan',
                    'surname' => 'smith',
                    'email' => 'johnsmith@mail.com',
                    'events' => [
                        [
                            'id' => $eventThree->id,
                            'name' => 'event three',
                            'city' => 'Rostov',
                            'event_date' => '2020-07-12 00:00:00'
                        ],
                        [
                            'id' => $eventFour->id,
                            'name' => 'event four',
                            'city' => 'Rostov',
                            'event_date' => '2020-07-12 00:00:00'
                        ]
                    ]
                ]
            ]);

        $updatedMember = Member::whereEmail('johnsmith@mail.com')->first();


        $this->assertEquals('Ivan', $updatedMember->name);
        $this->assertEquals('smith', $updatedMember->surname);
        $this->assertEquals(2, $updatedMember->events->count());

    }

    public function testCreateMemberAndAttachToEvent()
    {

       $eventThree = Event::add(
            'event three',
            'Rostov',
            Carbon::make('12.07.2020')
        );

        $responseError = $this->postJson('/api/creat-member-and-attach-to-event', [
            'name' => 'Jo',
            'surname' => 'Johnsmith',
            'email' => 'johnsmith@mail.com',
            'events_ids' => [$eventThree->id, 999]
        ]);

        $responseError
            ->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY)
            ->assertJsonPath('errors.name', ['Имя должно быть длиннее двух символов'])
            ->assertJsonPath('errors.events_ids', ['мероприятия или одного из мероприятий не найдено']);

        $response = $this->postJson('/api/creat-member-and-attach-to-event', [
            'name' => 'John',
            'surname' => 'Johnsmith',
            'email' => 'johnsmith@mail.com',
            'events_ids' => [$eventThree->id]
        ]);

        $member = Member::whereEmail('johnsmith@mail.com')->first();

        $response->assertStatus(201)
            ->assertExactJson([
                'data' => [
                    'id' => $member->id,
                    'name' => 'John',
                    'surname' => 'Johnsmith',
                    'email' => 'johnsmith@mail.com',
                    'events' => [
                        [
                            'id' => $eventThree->id,
                            'name' => 'event three',
                            'city' => 'Rostov',
                            'event_date' => '2020-07-12 00:00:00'
                        ]
                    ]
                ]
            ]);

        $member = Member::whereEmail('johnsmith@mail.com')->first();

        $this->assertEquals('johnsmith@mail.com', $member->email);
        $this->assertEquals('event three', $member->events->toArray()[0]['name']);

    }

    public function testDeleteMember()
    {
        $memberService = new MemberService();

        $eventThree = Event::add(
            'event three',
            'Rostov',
            Carbon::make('12.07.2020')
        );

        $member = $memberService->createMemberAndAttachToEvent('John', 'Johnsmith', 'johnsmith@mail.com', $eventThree->id);

        $response = $this->get('/api/member-list');

        $response->assertStatus(200)
            ->assertExactJson([
                'data' => [
                    [
                        'id' => $member->id,
                        'name' => 'John',
                        'surname' => 'Johnsmith',
                        'email' => 'johnsmith@mail.com',
                        'events' => [
                            [
                                'id' => $eventThree->id,
                                'name' => 'event three',
                                'city' => 'Rostov',
                                'event_date' => '2020-07-12 00:00:00'
                            ]
                        ]
                    ]
                ]
            ]);

        $this->delete('/api/member/'.$member->id);

        $deleteResponse = $this->get('/api/member-list');

        $deleteResponse->assertStatus(200)
            ->assertExactJson([
                'data' => []
            ]);
    }

    public function testMemberList()
    {
        $eOne = Event::add(
            'event one',
            'Rostov',
            Carbon::make('12.07.2020')
        );
        $eTwo = Event::add(
            'event two',
            'Moscow',
            Carbon::make('21.09.2020')
        );

        $ivanivanov = Member::add(
            'Ivan',
            'Ivanov',
            'ivanivanov@mail.com'
        );

        $ivanivanovEvent = Event::whereName('event one')->firstOrFail();

        $ivanivanov->events()->attach($ivanivanovEvent->id);

        $petrpetrov = Member::add(
            'Petr',
            'Petrov',
            'petrpetrov@mail.com'
        );

        $petrpetrovEvent = Event::whereName('event two')->firstOrFail();

        $petrpetrov->events()->attach($petrpetrovEvent->id);

        $response = $this->get('/api/member-list');

        $response->assertStatus(200)
            ->assertExactJson([
                'data' => [
                    [
                        'id' => $ivanivanov->id,
                        'name' => 'Ivan',
                        'surname' => 'Ivanov',
                        'email' => 'ivanivanov@mail.com',
                        'events' => [
                            [
                                'id' => $eOne->id,
                                'name' => 'event one',
                                'city' => 'Rostov',
                                'event_date' => '2020-07-12 00:00:00'
                            ]
                        ]

                    ],
                    [
                        'id' => $petrpetrov->id,
                        'name' => 'Petr',
                        'surname' => 'Petrov',
                        'email' => 'petrpetrov@mail.com',
                        'events' => [
                            [
                                'id' => $eTwo->id,
                                'name' => 'event two',
                                'city' => 'Moscow',
                                'event_date' => '2020-09-21 00:00:00'
                            ]
                        ]

                    ],
                ]
            ]);


        $eventThree = Event::add(
            'event three',
            'Moscow',
            Carbon::make('21.09.2020')
        );

        $memberService = new MemberService();

        $member = $memberService->createMemberAndAttachToEvent('John', 'Johnsmith', 'johnsmith@mail.com', $eventThree->id);

        $responseTwo = $this->get('/api/member-list?events-ids=' . $eventThree->id);

        $responseTwo->assertStatus(200)
            ->assertExactJson([
                'data' => [
                    [
                        'id' => $member->id,
                        'name' => 'John',
                        'surname' => 'Johnsmith',
                        'email' => 'johnsmith@mail.com',
                        'events' => [
                            [
                                'id' => $eventThree->id,
                                'name' => 'event three',
                                'city' => 'Moscow',
                                'event_date' => '2020-09-21 00:00:00'
                            ]
                        ]
                    ]
                ]
            ]);

    }}

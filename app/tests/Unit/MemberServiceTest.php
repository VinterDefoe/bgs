<?php

namespace Tests\Feature;

use App\Models\Event;
use App\Models\Member;
use App\UseCases\MemberService;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Carbon;
use Tests\TestCase;

class MemberServiceTest extends TestCase
{
    use RefreshDatabase;

    public function testupdateMemberAndMemberEvents()
    {

        $memberService = new MemberService();

        $eventThree = Event::add(
            'event three',
            'Rostov',
            Carbon::make('12.07.2020')
        );

        $eventFour = Event::add(
            'event four',
            'Rostov',
            Carbon::make('12.07.2020')
        );

        $memberService->createMemberAndAttachToEvent('John', 'Johnsmith', 'johnsmith@mail.com', $eventThree->id);

        $member = Member::whereEmail('johnsmith@mail.com')->first();

        $memberService->updateMemberAndMemberEvents($member->id,'Ivan', 'smith', 'johnsmith@mail.com', [$eventThree->id, $eventFour->id]);

        $updatedMember = Member::whereEmail('johnsmith@mail.com')->first();

        $this->assertEquals('Ivan', $updatedMember->name);
        $this->assertEquals('smith', $updatedMember->surname);
        $this->assertEquals(2, $updatedMember->events->count());


        $memberService->updateMemberAndMemberEvents($member->id,'Ivan', 'smith', 'johnsmith@mail.com', []);

        $updatedMemberTwo = Member::whereEmail('johnsmith@mail.com')->first();

        $this->assertEquals(0, $updatedMemberTwo->events->count());
    }


    public function testCreateMemberAndAttachToEvent()
    {

        $memberService = new MemberService();

        $eventThree = Event::add(
            'event three',
            'Rostov',
            Carbon::make('12.07.2020')
        );

        $memberService->createMemberAndAttachToEvent('John', 'Johnsmith', 'johnsmith@mail.com', $eventThree->id);

        $member = Member::whereEmail('johnsmith@mail.com')->first();

        $this->assertEquals('johnsmith@mail.com', $member->email);
        $this->assertEquals('event three', $member->events->toArray()[0]['name']);

    }

    public function testMemberList()
    {
        Event::add(
            'event one',
            'Rostov',
            Carbon::make('12.07.2020')
        );
        Event::add(
            'event two',
            'Moscow',
            Carbon::make('21.09.2020')
        );

        $ivanivanov = Member::add(
            'Ivan',
            'Ivanov',
            'ivanivanov@mail.com'
        );

        $ivanivanovEvent = Event::whereName('event one')->firstOrFail();

        $ivanivanov->events()->attach($ivanivanovEvent->id);

        $petrpetrov = Member::add(
            'Petr',
            'Petrov',
            'petrpetrov@mail.com'
        );

        $petrpetrovEvent = Event::whereName('event two')->firstOrFail();

        $petrpetrov->events()->attach($petrpetrovEvent->id);

        $memberService = new MemberService();

        $resultOne = $memberService->memberList(['events-ids' => $ivanivanovEvent->id]);

        $this->assertEquals('ivanivanov@mail.com', $resultOne->first()->email);
        $this->assertEquals(1, $resultOne->count());


        $resultTwo = $memberService->memberList(['events-ids' => '']);
        $this->assertEquals(2, $resultTwo->count());
    }

    
}

<?php

use App\Models\Event;
use App\Models\Member;
use Illuminate\Database\Seeder;

class MemberSeeder extends Seeder
{
    public function run()
    {
        $ivanivanov = Member::add(
            'Иван',
            'Иванов',
            'ivanivanov@mail.com'
        );

        $ivanivanovEvent = Event::whereName('GoWayFest 4.0 Online Edition Conference')->firstOrFail();

        $ivanivanov->events()->attach($ivanivanovEvent->id);

        $petrpetrov = Member::add(
            'Петр',
            'Петрив',
            'petrpetrov@mail.com'
        );

        $petrpetrovEvent = Event::whereName('C++ Russia 2020 Moscow')->firstOrFail();

        $petrpetrov->events()->attach($petrpetrovEvent->id);
    }
}

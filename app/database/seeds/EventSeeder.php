<?php

use App\Models\Event;
use Illuminate\Database\Seeder;
use Illuminate\Support\Carbon;

class EventSeeder extends Seeder
{
    public function run()
    {
        Event::add(
            'Предпринимательский форум «Pro_Таргетинг»',
            'Минск',
            Carbon::make('27.06.2021')
        );
        Event::add(
            'Внутреннее предпринимательство в большом бизнесе',
            'Билярск',
            Carbon::make('25.08.2020')
        );
        Event::add(
            'GoWayFest 4.0 Online Edition Conference',
            'Иркутск',
            Carbon::make('15.08.2020')
        );
        Event::add(
            'C++ Russia 2020 Moscow',
            'Ростов',
            Carbon::make('12.07.2020')
        );
        Event::add(
            'Автоматизация управления по целям и KPI в среднем и крупном бизнесе',
            'Москва',
            Carbon::make('21.09.2020')
        );
    }
}

up-prod:
	docker-compose -f docker-compose-production.yml build
	docker-compose -f docker-compose-production.yml up -d

up-dev:
	docker-compose build
	docker-compose up -d

docker-down:
	docker-compose down --remove-orphans

php-fpm:
	docker-compose exec php-fpm bash

node-install:
	docker-compose run --rm node npm install

node-prod:
	docker-compose run --rm node npm run prod

node-watch:
	docker-compose run --rm node npm run watch

php-docs-facades:
	docker-compose run --rm php-fpm php artisan ide-helper:generate

php-docs-meta:
	docker-compose run --rm php-fpm php artisan ide-helper:meta

php-docs-models:
	docker-compose run --rm php-fpm php artisan ide-helper:models

php-clear:
	docker-compose run --rm php-fpm php artisan optimize:clear

tests:
	docker-compose run --rm php-fpm php artisan test

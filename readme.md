Install prod:

        docker-compose -f docker-compose-production.yml build
        
    	docker-compose -f docker-compose-production.yml up -d
    	
    	docker-compose exec php-fpm php artisan db:seed
    	

Тесты в prod не запускаются, только в dev, для запуска больше действий

Для почты mailer на http://localhost:8081

Фронтенд для демонстрации http://localhost:80

Api example:

    get('/api/event-list');
    get('/event-list';
    
    get('/api/member-list?events-ids=1,2,3');
    get('/member-list')
    
    postJson('/api/creat-member-and-attach-to-event', [
                'name' => 'Jo',
                'surname' => 'Johnsmith',
                'email' => 'johnsmith@mail.com',
                'events_ids' => [1, 2]
            ]);
            
    post('/creat-member-and-attach-to-event')
    
    putJson('/api/update-member-and-member-events/' . $member->id, [
                'name' => 'Ivan',
                'surname' => 'smith',
                'email' => 'johnsmith@mail.com',
                'events_ids' => [1, 2]
            ]);
    put('/update-member-and-member-events/{id}')
    
    delete('/api/member/1');
    delete('/member/{id}')
    
    Создать API-приложение для управления участниками мероприятия.
    
     
    Участник содержит поля имя/фамилия/email и привязан к мероприятию
    
    Мероприятие содержит поля название/дата проведения/город (для них api не требуется)
    
    Возможности
    Добавлять/получать/изменять/удалять участников через http запрос
    
    Фильтрация данных при запросе (возвращать только участников определенного мероприятия)
    
     
    Требования
    Использование фреймворка lumen/laravel (можно использовать любые дополнительные пакеты)
    
    Доступ к API закрыт напрямую
    
    Должны быть unit тесты (все покрывать необязательно)
    
    Формат возвращаемых данных - json
    
    Мероприятия уже существуют в базе при запуске приложения
    
    При успешном создании нового участника эмулируется отправка email через очередь (можно писать в лог)
    
    Участник уникален по email
    
    Результат
    Ссылка на git-репозиторий, содержащий приложение, инструкции для его запуска, инструкции по работе с API
    
    Дополнительное задание (необязательно)
    Приложение и его составляющие запускаются внутри docker контейнеров